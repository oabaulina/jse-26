package ru.baulina.tm.exception.empty;

public class EmptyIdException extends RuntimeException{

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

}
