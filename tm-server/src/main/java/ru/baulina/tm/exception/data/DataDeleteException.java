package ru.baulina.tm.exception.data;

public class DataDeleteException extends RuntimeException {

    public DataDeleteException() {
        super("Error! Problem with delete file...");
    }
}
