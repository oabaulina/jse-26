package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IRepository;
import ru.baulina.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    private Class<E> clazz;

    private EntityManager entityManager;

    @NotNull
    @Override
    public List<E> findAll(){
        return entityManager
                .createQuery("FROM " + clazz.getName())
                .getResultList();
    }

    @Override
    public void setClazz(Class<E> clazzToSet) {
        this.clazz = clazzToSet;
    }

    @Override
    public void merge(@Nullable final List<E> entities) {
        if (entities == null) return;
        for (final E entity: entities) merge(entity);
    }

    @Override
    @SafeVarargs
    public final void merge(@Nullable final E... entities) {
        if (entities == null) return;
        for (final E entity: entities) merge(entity);
    }

    @Override
    public void merge(@Nullable final E entity) {
        if (entity == null) return;
        entityManager.merge(entity);
    }

    @Override
    public void save(E entity){
        entityManager.persist(entity);
    }

    @Override
    public void update(E entity){
        entityManager.merge(entity);
    }

    @Override
    public void clear() {
        @NotNull final List<E> entites = findAll();
        entites.forEach(entityManager::remove);
    }

    @NotNull
    @Override
    public Long count(){
        return (Long) entityManager
                .createQuery("SELECT count(t) FROM " + clazz.getName() + " t")
                .getSingleResult();
    }

}
