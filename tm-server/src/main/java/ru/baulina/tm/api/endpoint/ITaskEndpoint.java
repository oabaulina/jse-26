package ru.baulina.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.TaskDTO;
import ru.baulina.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    @NotNull List<TaskDTO> getTaskList();

    @WebMethod
    void loadTasks(
            @WebParam(name = "tasks", partName = "tasks") List<Task> tasks
    );

    @WebMethod
    TaskDTO createTask(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    TaskDTO createTaskWithDescription(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "task", partName = "task") Task task
    );

    @WebMethod
    List<TaskDTO> findAllTasks(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void clearTasks(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    TaskDTO findOneTaskById(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    TaskDTO findOneTaskByIndex(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    TaskDTO findOneTaskByName(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void removeOneTaskById(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    void removeOneTaskByIndex(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    void removeOneTaskByName(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void updateTaskById(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "id", partName = "id") Long id,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    void updateTaskByIndex(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") Long projectId,
            @WebParam(name = "index", partName = "index") Integer index,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    );

}
