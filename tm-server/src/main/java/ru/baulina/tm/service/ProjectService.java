package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.repository.IRepository;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.exception.entity.ProjectNotFoundException;
import ru.baulina.tm.exception.entity.TaskNotFoundException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;
import ru.baulina.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull private final PropertyService propertyService = new PropertyService();

    @NotNull private final EntityManagerFactoryService entityManagerFactoryService
            = new EntityManagerFactoryService(propertyService);

    @Override
    protected IRepository<Project> getRepository() {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        return new ProjectRepository(em);
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final User user,
            @Nullable final String name
    ) {
        if (user == null) throw new EmptyUserException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final Project project = new Project();
            project.setUser(user);
            project.setName(name);
            em.merge(project);
            em.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final User user,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (user == null) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final Project project = new Project();
            project.setUser(user);
            project.setName(name);
            project.setDescription(description);
            em.merge(project);
            em.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            repository.clear(userId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            @NotNull List<Project> listProject = repository.findAll(userId);
            em.getTransaction().commit();
            return listProject;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public List<Project> findListProjects() {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            @NotNull List<Project> listProject = repository.findListProjects();
            em.getTransaction().commit();
            return listProject;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneById(
            @Nullable final Long userId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            @Nullable Project project = repository.findOneById(userId, id);
            em.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByIndex(
            @Nullable final Long userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            @Nullable Project project = repository.findOneByIndex(userId, index);
            em.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByName(
            @Nullable final Long userId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            @Nullable Project project = repository.findOneByName(userId, name);
            em.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public void remove(@Nullable final Project project) {
        if (project == null) throw new EmptyProjectException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            repository.remove(project);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeOneById(
            @Nullable final Long userId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            @Nullable Project project = repository.removeOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeOneByIndex(
            @Nullable final Long userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            @Nullable Project project = repository.removeOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
     }

    @Override
    public void removeOneByName(
            @Nullable final Long userId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            @Nullable Project project = repository.removeOneByName(userId, name);
            if (project == null) throw new ProjectNotFoundException();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void updateProjectById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            final Project project = findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            project.setId(id);
            project.setName(name);
            project.setDescription(description);
            em.merge(project);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void updateProjectByIndex(
            @Nullable final Long userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IProjectRepository repository = new ProjectRepository(em);
            @Nullable final Project project = findOneByIndex(userId, index);
            if (project == null) throw new TaskNotFoundException();
            project.setName(name);
            project.setDescription(description);
            repository.merge(project);
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

}
