package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.endpoint.IAdminUserEndpoint;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.UserDTO;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
public class AdminUserEndpoint implements IAdminUserEndpoint {

    private IServiceLocator serviceLocator;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public List<UserDTO> getUserList(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final List<User> userList = serviceLocator.getUserService().findAll();
        @Nullable List<UserDTO> userListDTO = new ArrayList<>();
        userList.forEach((user) -> userListDTO.add((new UserDTO()).userDTOfrom(user)));
        return userListDTO;
    }

    @Override
    @WebMethod
    public UserDTO findUserById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") final Long id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final User user = serviceLocator.getUserService().findById(id);
        @Nullable UserDTO userDTO = new UserDTO();
        return userDTO.userDTOfrom(user);
    }

    @Override
    @WebMethod
    public UserDTO findUserByLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        @Nullable UserDTO userDTO = new UserDTO();
        return userDTO.userDTOfrom(user);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") final Long id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeById(id);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @WebMethod
    public void lockUserLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserLogin(login);
    }

}
