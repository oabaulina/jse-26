package ru.baulina.tm.exception.user;

public class AccessDeniedException extends RuntimeException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
