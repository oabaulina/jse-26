package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

public final class ProfileOfUserChangeCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "change-profiler-of-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Change user's profiler.";
    }

    @Override
    public void execute() {

        System.out.println("[CHANGE_PROFILE_OF_USER]");
        System.out.println("ENTER E-MAIL: ");
        final String newEmail = TerminalUtil.nextLine();
        System.out.println("ENTER FEST NAME: ");
        final String newFestName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        final String newLastName = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getUserEndpoint().profileOfUserChange(
                session, newEmail, newFestName, newLastName, session.getUserId()
        );
        System.out.println("[OK]");
        System.out.println();
    }

}
