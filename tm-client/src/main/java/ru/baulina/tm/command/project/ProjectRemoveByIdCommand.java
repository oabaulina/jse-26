package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID");
        @Nullable final Long id = TerminalUtil.nexLong();
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getProjectEndpoint().removeOneProjectById(session, id);
        System.out.println("[OK]");
        System.out.println();
    }

}
