package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.bootstrap.Bootstrap;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.user.AccessDeniedException;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.USER, Role.ADMIN};
    }

}
