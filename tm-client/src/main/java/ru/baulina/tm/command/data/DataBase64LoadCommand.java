package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.bootstrap.Bootstrap;
import ru.baulina.tm.endpoint.SessionDTO;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from base64 file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getAdminDampEndpoint().dataBase64Load(session);
        System.out.println("[LOGOUT CURRENT USER]");
        endpointLocator.getSessionEndpoint().closeSession(session);
        @NotNull final Bootstrap bootstrap = (Bootstrap) endpointLocator;
        bootstrap.setSession(null);
        System.out.println("[OK]");
        System.out.println();
    }

}
