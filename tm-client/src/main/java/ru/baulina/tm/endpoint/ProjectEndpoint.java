package ru.baulina.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-02-11T17:12:12.536+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.baulina.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/createProjectWithDescriptionRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/createProjectWithDescriptionResponse")
    @RequestWrapper(localName = "createProjectWithDescription", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.CreateProjectWithDescription")
    @ResponseWrapper(localName = "createProjectWithDescriptionResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.CreateProjectWithDescriptionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.baulina.tm.endpoint.ProjectDTO createProjectWithDescription(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/findAllProjectsRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/findAllProjectsResponse")
    @RequestWrapper(localName = "findAllProjects", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.FindAllProjects")
    @ResponseWrapper(localName = "findAllProjectsResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.FindAllProjectsResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.baulina.tm.endpoint.ProjectDTO> findAllProjects(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/findOneProjectByIndexRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/findOneProjectByIndexResponse")
    @RequestWrapper(localName = "findOneProjectByIndex", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.FindOneProjectByIndex")
    @ResponseWrapper(localName = "findOneProjectByIndexResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.FindOneProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.baulina.tm.endpoint.ProjectDTO findOneProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/loadProjectsRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/loadProjectsResponse")
    @RequestWrapper(localName = "loadProjects", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.LoadProjects")
    @ResponseWrapper(localName = "loadProjectsResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.LoadProjectsResponse")
    public void loadProjects(
        @WebParam(name = "projects", targetNamespace = "")
        java.util.List<ru.baulina.tm.endpoint.Project> projects
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/clearProjectsRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/clearProjectsResponse")
    @RequestWrapper(localName = "clearProjects", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.ClearProjects")
    @ResponseWrapper(localName = "clearProjectsResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.ClearProjectsResponse")
    public void clearProjects(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/findOneProjectByIdRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/findOneProjectByIdResponse")
    @RequestWrapper(localName = "findOneProjectById", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.FindOneProjectById")
    @ResponseWrapper(localName = "findOneProjectByIdResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.FindOneProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.baulina.tm.endpoint.ProjectDTO findOneProjectById(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.Long id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/removeOneProjectByIdRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/removeOneProjectByIdResponse")
    @RequestWrapper(localName = "removeOneProjectById", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.RemoveOneProjectById")
    @ResponseWrapper(localName = "removeOneProjectByIdResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.RemoveOneProjectByIdResponse")
    public void removeOneProjectById(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.Long id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/updateProjectByIdRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/updateProjectByIdResponse")
    @RequestWrapper(localName = "updateProjectById", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.UpdateProjectById")
    @ResponseWrapper(localName = "updateProjectByIdResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.UpdateProjectByIdResponse")
    public void updateProjectById(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.Long id,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/updateProjectByIndexRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/updateProjectByIndexResponse")
    @RequestWrapper(localName = "updateProjectByIndex", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.UpdateProjectByIndex")
    @ResponseWrapper(localName = "updateProjectByIndexResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.UpdateProjectByIndexResponse")
    public void updateProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/removeProjectRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/removeProjectResponse")
    @RequestWrapper(localName = "removeProject", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.RemoveProject")
    @ResponseWrapper(localName = "removeProjectResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.RemoveProjectResponse")
    public void removeProject(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "project", targetNamespace = "")
        ru.baulina.tm.endpoint.Project project
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/findOneProjectByNameRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/findOneProjectByNameResponse")
    @RequestWrapper(localName = "findOneProjectByName", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.FindOneProjectByName")
    @ResponseWrapper(localName = "findOneProjectByNameResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.FindOneProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.baulina.tm.endpoint.ProjectDTO findOneProjectByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/removeOneProjectByIndexRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/removeOneProjectByIndexResponse")
    @RequestWrapper(localName = "removeOneProjectByIndex", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.RemoveOneProjectByIndex")
    @ResponseWrapper(localName = "removeOneProjectByIndexResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.RemoveOneProjectByIndexResponse")
    public void removeOneProjectByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/createProjectRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/createProjectResponse")
    @RequestWrapper(localName = "createProject", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.CreateProject")
    @ResponseWrapper(localName = "createProjectResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.CreateProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.baulina.tm.endpoint.ProjectDTO createProject(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/removeOneProjectByNameRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/removeOneProjectByNameResponse")
    @RequestWrapper(localName = "removeOneProjectByName", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.RemoveOneProjectByName")
    @ResponseWrapper(localName = "removeOneProjectByNameResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.RemoveOneProjectByNameResponse")
    public void removeOneProjectByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.baulina.tm.endpoint.SessionDTO session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.baulina.ru/ProjectEndpoint/getProjectListRequest", output = "http://endpoint.tm.baulina.ru/ProjectEndpoint/getProjectListResponse")
    @RequestWrapper(localName = "getProjectList", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.GetProjectList")
    @ResponseWrapper(localName = "getProjectListResponse", targetNamespace = "http://endpoint.tm.baulina.ru/", className = "ru.baulina.tm.endpoint.GetProjectListResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.baulina.tm.endpoint.ProjectDTO> getProjectList();
}
